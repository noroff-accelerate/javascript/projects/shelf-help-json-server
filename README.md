# JSON Server for Shelf Help App

## Overview

This JSON Server acts as a mock backend for the Shelf Help application, a platform for users to explore, manage, and curate a personal collection of books. It provides a RESTful API for performing CRUD operations on a simulated database of books, using a simple JSON file (`db.json`).

## Features

- **Books Endpoint**: Offers a collection of books with details such as title, author, ISBN, cover image, and blurb.
- **Users Endpoint**: Offers a collection of users with details such as username and collection.
- **Modifiable Data**: Unlike the initial read-only setup, this server allows for creating, updating, and deleting records, enabling a dynamic experience for managing book data.

## Data Structure

The `db.json` file contains an initial dataset under the `books` `users` and key. 

Each book object includes:
- `id`: A unique identifier for the book.
- `isbn`: The International Standard Book Number.
- `title`: The title of the book.
- `author`: The author's name.
- `coverImg`: A filename for the cover image.
- `blurb`: A short description or synopsis of the book.

Each user object includes:
- `id`: A unique identifier for the user.
- `username`: The users username.
- `collection`: The collection of the book titles.

## Setup and Running the Server

### Prerequisites

- Node.js and npm (Node Package Manager)

### Installation

1. Clone the repository to your local machine, or download the provided files into a directory of your choice.
2. Navigate to the directory containing the `db.json` file and the server script via the terminal or command prompt.
3. Install the project dependencies by running: npm install
4. Run the server: npm start

### Usage
Once the server is running, you can access the books or user data by visiting http://localhost:3000/books (or /user) in your web browser, or by using tools like Postman or fetch to make GET, POST, PUT, and DELETE requests to interact with the data.